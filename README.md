# 返利快车H5链接

部署后替换域名即可测试

#### 首页

>chipId：酷赛唯一ID

```
http://localhost:59310/#HomePage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333
```

#### 搜索

>chipId：酷赛唯一ID
>
>keyword: 搜索词
>
>platformType：搜索平台（1：淘宝，2：京东，3：拼多多）

```
http://localhost:59310/#SearchPrePage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333&keyword=可乐&platformType=2
```

#### 二级页

>chipId：酷赛唯一ID

- 拼多多
```
http://localhost:59310/#PddActivityPage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333
```
- 淘宝
```
http://localhost:59310/#TbActivityPage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333
```
- 京东
```
http://localhost:59310/#JdActivityPage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333
```

#### 提现记录

>chipId：酷赛唯一ID

```
http://localhost:59310/#WithdrawalRecordPage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333
```

#### 订单详情

>chipId：酷赛唯一ID
>
>orderId: 订单ID

```
http://localhost:59310/#OrderDetailPage?chipId=931d86d16ff8d4d32647faa97f5a26fe924f4c7575f280bbefef8c7d5e610333&orderId=5
```

# 返利快车deep link链接

使用以下步骤：

**1. 构建params**

- 搜索页：
  >keyword搜索词
  >
  >platformType搜索平台（1：淘宝，2：京东，3：拼多多)

  params:```{"category": "jump", "url": "SearchPrePage", "keyword": "可乐", "platformType": 2}```<br>

- 订单详情页：
  >orderId订单ID

  params: ```{"category": "jump", "url": "OrderDetailPage", "orderId": 10171}```<br>

- 拼多多二级页：
  >needLogin(可选参数，true表示进入前需要唤起登录页，如果已经登录则不唤起)

  params:``` {"category": "jump", "url": "PddActivityPage", "needLogin": true}```<br>
- 淘宝二级页：
  >needLogin(可选参数，true表示进入前需要唤起登录页，如果已经登录则不唤起)

  params:``` {"category": "jump", "url": "TbActivityPage", "needLogin": true}```<br>
- 京东二级页：
  >needLogin(可选参数，true表示进入前需要唤起登录页，如果已经登录则不唤起)

  params:``` {"category": "jump", "url": "JdActivityPage", "needLogin": true}```<br>

- 提现记录页：<br>
  params:``` {"category": "jump", "url": "WithdrawalRecordPage"}```<br>

- 0元购：
  >needLogin(表示进入前需要唤起登录页，如果已经登录则不唤起)

  params:``` {"category": "jump", "url": "ZeroPurchaseActivityPage", "needLogin": true}```<br>

- 首页和我的页：
  >index(0：首页，1：我的)

  params: ```{"category": "switch", "index": 1}```

**2. 对params进行encode**

例：

```{"category": "jump", "url": "SearchPrePage", "keyword": "可乐", "platformType": 2}```

encode后：

```
7B%22category%22%3A%20%22jump%22%2C%20%22url%22%3A%20%22SearchPrePage%22%2C%20%22keyword%22%3A%20%22%E5%8F%AF%E4%B9%90%22%2C%20%22platformType%22%3A%202%7D
```

拼接后：

```
flkc.msmds://virtual?params=%7B%22category%22%3A%20%22jump%22%2C%20%22url%22%3A%20%22SearchPrePage%22%2C%20%22keyword%22%3A%20%22%E5%8F%AF%E4%B9%90%22%2C%20%22platformType%22%3A%202%7D
```

**3. 测试**

```
adb shell am start -a android.intent.action.VIEW -d "flkc.msmds://virtual?params=%7B%22category%22%3A%20%22jump%22%2C%20%22url%22%3A%20%22SearchPrePage%22%2C%20%22keyword%22%3A%20%22%E5%8F%AF%E4%B9%90%22%2C%20%22platformType%22%3A%202%7D"
```
